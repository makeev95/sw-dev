package deadlock;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Deadlock {

    private int diff = 0;
    private final Map<Account, Lock> accounts = new HashMap<>();
    {
        for (int i = 0; i < 100; i++) {
            Account account = new Account(i);
            diff += account.getBalance();
            accounts.put(account, new ReentrantLock());
        }
    }

    public void execute() {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            Account acc1 = new Account(random.nextInt(100));
            Account acc2 = new Account(random.nextInt(100));
            Lock lock1 = accounts.get(acc1);
            Lock lock2 = accounts.get(acc2);

            lock1.lock();
            lock2.lock();
//            acquireLocks(lock1, lock2);
            try {
                Account.transfer(acc1, acc2, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public int getDiff() {
        for (Map.Entry<Account, Lock> accountLockEntry : accounts.entrySet()) {
            diff -= accountLockEntry.getKey().getBalance();
        }
        return diff;
    }

    private void acquireLocks(Lock lock1, Lock lock2) {
        while (true) {
            boolean gotFirstLock = false;
            boolean gotSecondLock = false;

            try {
                gotFirstLock = lock1.tryLock();
                gotSecondLock = lock2.tryLock();
            } finally {
                if (gotFirstLock && gotSecondLock)
                    return;
                if (gotFirstLock) lock1.unlock();
                if (gotSecondLock) lock2.unlock();
            }
        }
    }

}
