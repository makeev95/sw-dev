package deadlock;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class DeadlockTest {

    @Test
    public void test() throws Exception {
        for (int i = 0; i < 1000; i++) {
            final Deadlock deadlock = new Deadlock();
            ExecutorService executor  = Executors.newFixedThreadPool(4);
            executor.submit(deadlock::execute);
            executor.submit(deadlock::execute);
            executor.submit(deadlock::execute);
            executor.submit(deadlock::execute);
            executor.shutdown();
            executor.awaitTermination(1L, TimeUnit.DAYS);
            assertEquals(deadlock.getDiff(), 0);
        }
    }
}